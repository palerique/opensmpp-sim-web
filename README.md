[![pipeline status](https://gitlab.com/palerique/opensmpp-sim-web/badges/master/pipeline.svg)](https://gitlab.com/palerique/opensmpp-sim-web/commits/master)

You can use buildAndRun.sh script to build and execute the app.

After that access http://localhost:8484/swagger-ui.html to see the api and use it.
