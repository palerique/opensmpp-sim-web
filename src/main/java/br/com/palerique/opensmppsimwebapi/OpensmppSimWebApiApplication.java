package br.com.palerique.opensmppsimwebapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpensmppSimWebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpensmppSimWebApiApplication.class, args);
	}
}
