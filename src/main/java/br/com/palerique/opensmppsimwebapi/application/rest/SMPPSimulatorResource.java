package br.com.palerique.opensmppsimwebapi.application.rest;

import br.com.palerique.opensmppsimwebapi.domain.service.ApplicationShortMessageStore;
import br.com.palerique.opensmppsimwebapi.infrastructure.dto.ClientsDto;
import br.com.palerique.opensmppsimwebapi.infrastructure.dto.MessageDto;
import br.com.palerique.opensmppsimwebapi.infrastructure.exception.ApplicationException;
import br.com.palerique.opensmppsimwebapi.infrastructure.exception.ClientStoppedException;
import br.com.palerique.opensmppsimwebapi.infrastructure.exception.SendMessageException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.smpp.debug.Event;
import org.smpp.debug.FileEvent;
import org.smpp.pdu.DeliverSM;
import org.smpp.pdu.PDUException;
import org.smpp.pdu.WrongLengthOfStringException;
import org.smpp.smscsim.DeliveryInfoSender;
import org.smpp.smscsim.PDUProcessorGroup;
import org.smpp.smscsim.SMSCListener;
import org.smpp.smscsim.SMSCListenerImpl;
import org.smpp.smscsim.SMSCSession;
import org.smpp.smscsim.SimulatorPDUProcessor;
import org.smpp.smscsim.SimulatorPDUProcessorFactory;
import org.smpp.smscsim.util.Table;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class SMPPSimulatorResource {

  private static final Set<Integer> ports = new HashSet<>();
  private static final String dbgDir = "./";
  private static final String usersFileName = "users.txt";
  private static final Event event = new FileEvent(dbgDir, "sim.evt");

  private SMSCListener smscListener;
  private PDUProcessorGroup processors;
  private ApplicationShortMessageStore messageStore;
  private DeliveryInfoSender deliveryInfoSender;

  @PostConstruct
  public void startDefaultServer() throws IOException {
    startServer(2775);
  }

  /**
   * Permits a user to choose the port where to listen on and then creates and starts new instance of
   * <code>SMSCListener</code>. An instance of the <code>SimulatorPDUProcessor</code> is created and this instance is
   * passed to the <code>SMSCListener</code> which is started just after.
   */
  @RequestMapping(method = RequestMethod.POST, value = "/server")
  public void startServer(@RequestParam(defaultValue = "2775") int port) throws IOException {

    if (ports.contains(port)) {
      throw new ApplicationException("Port already in use");
    }

    log.info("Starting listener... ");
    smscListener = new SMSCListenerImpl(port, true);
    processors = new PDUProcessorGroup();
    messageStore = new ApplicationShortMessageStore();
    deliveryInfoSender = new DeliveryInfoSender();
    deliveryInfoSender.start();
    Table users = new Table(usersFileName);
    SimulatorPDUProcessorFactory factory =
        new SimulatorPDUProcessorFactory(processors, messageStore, deliveryInfoSender, users);
    boolean displayInfo = true;
    factory.setDisplayInfo(displayInfo);
    smscListener.setPDUProcessorFactory(factory);
    smscListener.start();
    ports.add(port);
    log.info("started.");
  }

  /**
   * Stops all the currently active sessions and then stops the listener.
   */
  @RequestMapping(method = RequestMethod.DELETE, value = "/server")
  public void stop() throws IOException {
    if (smscListener != null) {
      log.info("Stopping listener...");
      synchronized (processors) {
        int procCount = processors.count();
        SimulatorPDUProcessor proc;
        SMSCSession session;
        for (int i = 0; i < procCount; i++) {
          proc = (SimulatorPDUProcessor) processors.get(i);
          session = proc.getSession();
          log.info("Stopping session {}: {} ...", i, proc.getSystemId());
          session.stop();
          log.info(" stopped.");
        }
      }
      smscListener.stop();
      smscListener = null;
      if (deliveryInfoSender != null) {
        deliveryInfoSender.stop();
      }
      ports.clear();
      log.info("Stopped.");
    }
  }

  /**
   * Permits data to be sent to a specific client. With the id of the client set by the user, the method
   * <code>sendMessage</code> gets back the specific reference to the client's <code>PDUProcessor</code>. With this
   * reference you are able to send data to the client.
   */
  @RequestMapping(method = RequestMethod.POST, value = "/server/messages")
  public void sendMessage(@RequestParam(defaultValue = "12345") String client,
      @RequestParam(defaultValue = "A nice SMS message") String message) {
    if (smscListener != null) {
      int procCount = processors.count();
      if (procCount > 0) {
        SimulatorPDUProcessor proc;
        listClients();
        if (procCount <= 1) {
          proc = (SimulatorPDUProcessor) processors.get(0);
          client = proc.getSystemId();
        }
        for (int i = 0; i < procCount; i++) {
          proc = (SimulatorPDUProcessor) processors.get(i);
          if (proc.getSystemId().equals(client)) {
            if (proc.isActive()) {
              DeliverSM request = new DeliverSM();
              try {
                request.setShortMessage(message);
                proc.serverRequest(request);
                log.info("Message sent.");
              } catch (WrongLengthOfStringException e) {
                event.write(e, "");
                throw new SendMessageException("Message sending failed", e);
              } catch (IOException | PDUException ioe) {
                throw new SendMessageException("Message sending failed", ioe);
              }
            } else {
              throw new SendMessageException("This session is inactive.");
            }
          }
        }
      } else {
        log.error("No client connected.");
      }
    } else {
      log.error("You must start listener first.");
    }
  }

  /**
   * Prints all currently connected clients on the standard output.
   */
  @RequestMapping(method = RequestMethod.GET, value = "/server/clients")
  public Set<ClientsDto> listClients() {
    if (smscListener != null) {
      synchronized (processors) {
        Set<ClientsDto> result = new HashSet<>();
        int procCount = processors.count();
        SimulatorPDUProcessor proc;
        for (int i = 0; i < procCount; i++) {
          proc = (SimulatorPDUProcessor) processors.get(i);
          result.add(ClientsDto.builder()
              .id(proc.getSystemId())
              .active(proc.isActive())
              .build());
        }
        return result;
      }
    } else {
      throw new ClientStoppedException();
    }
  }

  /**
   * Prints all messages currently present in the message store on the standard output.
   */
  @RequestMapping(method = RequestMethod.GET, value = "/server/messages")
  public Collection<MessageDto> messageList() {
    if (smscListener != null) {
      return messageStore.getMessages();
    } else {
      throw new ClientStoppedException();
    }
  }
}
