package br.com.palerique.opensmppsimwebapi.domain.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.smpp.pdu.SubmitSM;
import org.smpp.smscsim.ShortMessageStore;

import br.com.palerique.opensmppsimwebapi.infrastructure.dto.MessageDto;

public class ApplicationShortMessageStore extends ShortMessageStore {

    private Map<String, MessageDto> messages = new HashMap<>();

    @Override
    public synchronized void submit(SubmitSM message, String messageId, String systemId) {
        super.submit(message, messageId, systemId);
        messages.put(messageId, getMessageDto(message, systemId));
    }

    private MessageDto getMessageDto(SubmitSM message, String systemId) {
        return MessageDto.builder()
                .systemId(systemId)
                .serviceType(message.getServiceType())
                .sourceAddress(message.getSourceAddr().getAddress())
                .destinationAddress(message.getDestAddr().getAddress())
                .message(message.getShortMessage())
                .build();
    }

    @Override
    public synchronized void cancel(String messageId) {
        super.cancel(messageId);
        messages.remove(messageId);
    }

    @Override
    public synchronized void replace(String messageId, String newMessage) {
        super.replace(messageId, newMessage);
        MessageDto messageDto = messages.get(messageId);
        if (messageDto != null) {
            messageDto.setMessage(newMessage);
        }
    }

    public Collection<MessageDto> getMessages() {
        return messages.values();
    }
}
