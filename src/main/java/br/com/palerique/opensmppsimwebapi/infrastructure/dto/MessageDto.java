package br.com.palerique.opensmppsimwebapi.infrastructure.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MessageDto {

    private String systemId;
    private String id;
    private String sender;
    private String serviceType;
    private String sourceAddress;
    private String destinationAddress;
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }
}
