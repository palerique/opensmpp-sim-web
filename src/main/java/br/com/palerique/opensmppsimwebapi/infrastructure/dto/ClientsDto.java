package br.com.palerique.opensmppsimwebapi.infrastructure.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ClientsDto {

    private String id;
    private Boolean active;
}
