package br.com.palerique.opensmppsimwebapi.infrastructure.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = ClientStoppedException.MESSAGE)
public class ClientStoppedException extends ApplicationException {

    public static final String MESSAGE = "You must start listener first.";

    public ClientStoppedException() {
        super(MESSAGE);
    }
}
