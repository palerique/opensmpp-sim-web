package br.com.palerique.opensmppsimwebapi.infrastructure.dto;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class ErrorDTO {

    private int code;
    private String message;
    private String detail;
}
