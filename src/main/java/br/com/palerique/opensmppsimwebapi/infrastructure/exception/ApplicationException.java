package br.com.palerique.opensmppsimwebapi.infrastructure.exception;

import java.util.Collections;
import java.util.List;

public class ApplicationException extends RuntimeException {

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Exception cause) {
        super(message, cause);
    }

    public List<String> getParams() {
        return Collections.emptyList();
    }
}
