package br.com.palerique.opensmppsimwebapi.infrastructure.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = SendMessageException.MESSAGE)
public class SendMessageException extends ApplicationException {

    static final String MESSAGE = "Impossible to send the message.";

    public SendMessageException(String message, Exception e) {
        super(String.format("%s %s", MESSAGE, message), e);
    }

    public SendMessageException(String message) {
        super(message);
    }
}
