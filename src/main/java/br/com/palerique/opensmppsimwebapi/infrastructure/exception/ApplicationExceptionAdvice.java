package br.com.palerique.opensmppsimwebapi.infrastructure.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.palerique.opensmppsimwebapi.infrastructure.dto.ErrorDTO;

@ControllerAdvice
@Slf4j
public class ApplicationExceptionAdvice {

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorDTO> handle(Exception exception) {
        log.error("Error during request processing", exception);
        HttpStatus status;
        String message;
        ResponseStatus responseStatus = AnnotationUtils.findAnnotation(exception.getClass(), ResponseStatus.class);
        if (responseStatus != null) {
            status = responseStatus.code();
            message = responseStatus.reason();
            if (exception instanceof ApplicationException) {
                message = String.format(message,
                        ((ApplicationException) exception).getParams().toArray());
            }
        } else {
            status = HttpStatus.BAD_REQUEST;
            message = prepareDetailMessageFromErrorObject(exception.getMessage());
        }

        return new ResponseEntity<>(ErrorDTO.builder()
                .detail(exception.fillInStackTrace().toString())
                .message(message)
                .code(status.value())
                .build(), status);
    }

    private String prepareDetailMessageFromErrorObject(String msg) {
        return String.join(" : ", "Invalid Attribute passed ", msg);
    }
}
